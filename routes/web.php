<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\{User, Article};

Route::get('/', function () {
//    $user = User::find(1);
//
//    $user->update([
//        'name' => 'Cristian',
//    ]);

    $article = Article::find(1);

    $article->update([
        'body' => 'This is a new body',
    ]);
});

Route::get('/users/{user}/history', function (User $user) {
    return view('users.history', [
        'histories' => $user->history
    ]);
});

    Route::get('/articles/{article}/history', function (Article $article) {
        return view('articles.history', [
            'histories' => $article->history
        ]);
    });


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
